# t7pro/constants.py

ORGANIZATION = "FEMTO_Engineering"
APP_NAME = "T7Logger"
APP_BRIEF = "GUI dedicated to handle the T7Pro DAQ board"
AUTHOR_NAME = "Benoit Dubois"
AUTHOR_MAIL = "benoit.dubois@femto-st.fr"
COPYRIGHT = "FEMTO Engineering"
LICENSE = "GNU GPL v3.0 or upper."
